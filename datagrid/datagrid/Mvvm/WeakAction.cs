﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace datagrid.MVVM
{
    public class WeakAction
    {
        private Action _staticAction;

        //
        // 摘要:
        //     Gets or sets the System.Reflection.MethodInfo corresponding to this WeakAction's
        //     method passed in the constructor.
        protected MethodInfo Method { get; set; }

        //
        // 摘要:
        //     Gets the name of the method that this WeakAction represents.
        public virtual string MethodName
        {
            get
            {
                if (_staticAction != null)
                {
                    return _staticAction.Method.Name;
                }

                return Method.Name;
            }
        }

        //
        // 摘要:
        //     Gets or sets a WeakReference to this WeakAction's action's target. This is not
        //     necessarily the same as GalaSoft.MvvmLight.Helpers.WeakAction.Reference, for
        //     example if the method is anonymous.
        protected WeakReference ActionReference { get; set; }

        //
        // 摘要:
        //     Saves the GalaSoft.MvvmLight.Helpers.WeakAction.ActionReference as a hard reference.
        //     This is used in relation with this instance's constructor and only if the constructor's
        //     keepTargetAlive parameter is true.
        protected object LiveReference { get; set; }

        //
        // 摘要:
        //     Gets or sets a WeakReference to the target passed when constructing the WeakAction.
        //     This is not necessarily the same as GalaSoft.MvvmLight.Helpers.WeakAction.ActionReference,
        //     for example if the method is anonymous.
        protected WeakReference Reference { get; set; }

        //
        // 摘要:
        //     Gets a value indicating whether the WeakAction is static or not.
        public bool IsStatic => _staticAction != null;

        //
        // 摘要:
        //     Gets a value indicating whether the Action's owner is still alive, or if it was
        //     collected by the Garbage Collector already.
        public virtual bool IsAlive
        {
            get
            {
                if (_staticAction == null && Reference == null && LiveReference == null)
                {
                    return false;
                }

                if (_staticAction != null)
                {
                    if (Reference != null)
                    {
                        return Reference.IsAlive;
                    }

                    return true;
                }

                if (LiveReference != null)
                {
                    return true;
                }

                if (Reference != null)
                {
                    return Reference.IsAlive;
                }

                return false;
            }
        }

        //
        // 摘要:
        //     Gets the Action's owner. This object is stored as a System.WeakReference.
        public object Target
        {
            get
            {
                if (Reference == null)
                {
                    return null;
                }

                return Reference.Target;
            }
        }

        //
        // 摘要:
        //     The target of the weak reference.
        protected object ActionTarget
        {
            get
            {
                if (LiveReference != null)
                {
                    return LiveReference;
                }

                if (ActionReference == null)
                {
                    return null;
                }

                return ActionReference.Target;
            }
        }

        //
        // 摘要:
        //     Initializes an empty instance of the GalaSoft.MvvmLight.Helpers.WeakAction class.
        protected WeakAction()
        {
        }

        //
        // 摘要:
        //     Initializes a new instance of the GalaSoft.MvvmLight.Helpers.WeakAction class.
        //
        // 参数:
        //   action:
        //     The action that will be associated to this instance.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is using closures. See http://galasoft.ch/s/mvvmweakaction.
        public WeakAction(Action action, bool keepTargetAlive = false)
            : this(action?.Target, action, keepTargetAlive)
        {
        }

        //
        // 摘要:
        //     Initializes a new instance of the GalaSoft.MvvmLight.Helpers.WeakAction class.
        //
        // 参数:
        //   target:
        //     The action's owner.
        //
        //   action:
        //     The action that will be associated to this instance.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is using closures. See http://galasoft.ch/s/mvvmweakaction.
        public WeakAction(object target, Action action, bool keepTargetAlive = false)
        {
            if (action.Method.IsStatic)
            {
                _staticAction = action;
                if (target != null)
                {
                    Reference = new WeakReference(target);
                }
            }
            else
            {
                Method = action.Method;
                ActionReference = new WeakReference(action.Target);
                LiveReference = (keepTargetAlive ? action.Target : null);
                Reference = new WeakReference(target);
            }
        }

        //
        // 摘要:
        //     Executes the action. This only happens if the action's owner is still alive.
        public void Execute()
        {
            if (_staticAction != null)
            {
                _staticAction();
                return;
            }

            object actionTarget = ActionTarget;
            if (IsAlive && Method != null && (LiveReference != null || ActionReference != null) && actionTarget != null)
            {
                Method.Invoke(actionTarget, null);
            }
        }

        //
        // 摘要:
        //     Sets the reference that this instance stores to null.
        public void MarkForDeletion()
        {
            Reference = null;
            ActionReference = null;
            LiveReference = null;
            Method = null;
            _staticAction = null;
        }
    }

    public class WeakAction<T> : WeakAction, IExecuteWithObject
    {
        private Action<T> _staticAction;

        //
        // 摘要:
        //     Gets the name of the method that this WeakAction represents.
        public override string MethodName
        {
            get
            {
                if (_staticAction != null)
                {
                    return _staticAction.Method.Name;
                }

                return base.Method.Name;
            }
        }

        //
        // 摘要:
        //     Gets a value indicating whether the Action's owner is still alive, or if it was
        //     collected by the Garbage Collector already.
        public override bool IsAlive
        {
            get
            {
                if (_staticAction == null && base.Reference == null)
                {
                    return false;
                }

                if (_staticAction != null)
                {
                    if (base.Reference != null)
                    {
                        return base.Reference.IsAlive;
                    }

                    return true;
                }

                return base.Reference.IsAlive;
            }
        }

        //
        // 摘要:
        //     Initializes a new instance of the WeakAction class.
        //
        // 参数:
        //   action:
        //     The action that will be associated to this instance.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is using closures. See http://galasoft.ch/s/mvvmweakaction.
        public WeakAction(Action<T> action, bool keepTargetAlive = false)
            : this(action?.Target, action, keepTargetAlive)
        {
        }

        //
        // 摘要:
        //     Initializes a new instance of the WeakAction class.
        //
        // 参数:
        //   target:
        //     The action's owner.
        //
        //   action:
        //     The action that will be associated to this instance.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is using closures. See http://galasoft.ch/s/mvvmweakaction.
        public WeakAction(object target, Action<T> action, bool keepTargetAlive = false)
        {
            if (action.Method.IsStatic)
            {
                _staticAction = action;
                if (target != null)
                {
                    base.Reference = new WeakReference(target);
                }
            }
            else
            {
                base.Method = action.Method;
                base.ActionReference = new WeakReference(action.Target);
                base.LiveReference = (keepTargetAlive ? action.Target : null);
                base.Reference = new WeakReference(target);
            }
        }

        //
        // 摘要:
        //     Executes the action. This only happens if the action's owner is still alive.
        //     The action's parameter is set to default(T).
        public new void Execute()
        {
            Execute(default(T));
        }

        //
        // 摘要:
        //     Executes the action. This only happens if the action's owner is still alive.
        //
        // 参数:
        //   parameter:
        //     A parameter to be passed to the action.
        public void Execute(T parameter)
        {
            if (_staticAction != null)
            {
                _staticAction(parameter);
                return;
            }

            object actionTarget = base.ActionTarget;
            if (IsAlive && base.Method != null && (base.LiveReference != null || base.ActionReference != null) && actionTarget != null)
            {
                base.Method.Invoke(actionTarget, new object[1] { parameter });
            }
        }

        //
        // 摘要:
        //     Executes the action with a parameter of type object. This parameter will be casted
        //     to T. This method implements GalaSoft.MvvmLight.Helpers.IExecuteWithObject.ExecuteWithObject(System.Object)
        //     and can be useful if you store multiple WeakAction{T} instances but don't know
        //     in advance what type T represents.
        //
        // 参数:
        //   parameter:
        //     The parameter that will be passed to the action after being casted to T.
        public void ExecuteWithObject(object parameter)
        {
            T parameter2 = (T)parameter;
            Execute(parameter2);
        }

        //
        // 摘要:
        //     Sets all the actions that this WeakAction contains to null, which is a signal
        //     for containing objects that this WeakAction should be deleted.
        public new void MarkForDeletion()
        {
            _staticAction = null;
            base.MarkForDeletion();
        }
    }

}
