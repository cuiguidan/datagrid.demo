﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datagrid.MVVM
{
    public interface IExecuteWithObject
    {
        //
        // 摘要:
        //     The target of the WeakAction.
        object Target { get; }

        //
        // 摘要:
        //     Executes an action.
        //
        // 参数:
        //   parameter:
        //     A parameter passed as an object, to be casted to the appropriate type.
        void ExecuteWithObject(object parameter);

        //
        // 摘要:
        //     Deletes all references, which notifies the cleanup method that this entry must
        //     be deleted.
        void MarkForDeletion();
    }
}
