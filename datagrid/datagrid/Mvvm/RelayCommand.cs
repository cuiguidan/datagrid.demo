﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace datagrid.MVVM
{
    public class RelayCommand : ICommand
    {
        private readonly WeakAction _execute;

        private readonly WeakFunc<bool> _canExecute;

        private EventHandler _requerySuggestedLocal;

        //
        // 摘要:
        //     Occurs when changes occur that affect whether the command should execute.
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                {
                    EventHandler eventHandler = _requerySuggestedLocal;
                    EventHandler eventHandler2;
                    do
                    {
                        eventHandler2 = eventHandler;
                        EventHandler value2 = (EventHandler)Delegate.Combine(eventHandler2, value);
                        eventHandler = Interlocked.CompareExchange(ref _requerySuggestedLocal, value2, eventHandler2);
                    }
                    while (eventHandler != eventHandler2);
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (_canExecute != null)
                {
                    EventHandler eventHandler = _requerySuggestedLocal;
                    EventHandler eventHandler2;
                    do
                    {
                        eventHandler2 = eventHandler;
                        EventHandler value2 = (EventHandler)Delegate.Remove(eventHandler2, value);
                        eventHandler = Interlocked.CompareExchange(ref _requerySuggestedLocal, value2, eventHandler2);
                    }
                    while (eventHandler != eventHandler2);
                    CommandManager.RequerySuggested -= value;
                }
            }
        }


        public RelayCommand(Action execute, bool keepTargetAlive = false)
            : this(execute, null, keepTargetAlive)
        {
        }

        public RelayCommand(Action execute, Func<bool> canExecute, bool keepTargetAlive = false)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }

            _execute = new WeakAction(execute, keepTargetAlive);
            if (canExecute != null)
            {
                _canExecute = new WeakFunc<bool>(canExecute, keepTargetAlive);
            }
        }


        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        public bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            {
                if (_canExecute.IsStatic || _canExecute.IsAlive)
                {
                    return _canExecute.Execute();
                }

                return false;
            }

            return true;
        }


        public virtual void Execute(object parameter)
        {
            if (CanExecute(parameter) && _execute != null && (_execute.IsStatic || _execute.IsAlive))
            {
                _execute.Execute();
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly WeakAction<T> _execute;

        private readonly WeakFunc<T, bool> _canExecute;

        //
        // 摘要:
        //     Occurs when changes occur that affect whether the command should execute.
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (_canExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }

        //
        // 摘要:
        //     Initializes a new instance of the RelayCommand class that can always execute.
        //
        // 参数:
        //   execute:
        //     The execution logic. IMPORTANT: If the action causes a closure, you must set
        //     keepTargetAlive to true to avoid side effects.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is causing a closure. See http://galasoft.ch/s/mvvmweakaction.
        //
        // 异常:
        //   T:System.ArgumentNullException:
        //     If the execute argument is null.
        public RelayCommand(Action<T> execute, bool keepTargetAlive = false)
            : this(execute, (Func<T, bool>)null, keepTargetAlive)
        {
        }

        //
        // 摘要:
        //     Initializes a new instance of the RelayCommand class.
        //
        // 参数:
        //   execute:
        //     The execution logic. IMPORTANT: If the action causes a closure, you must set
        //     keepTargetAlive to true to avoid side effects.
        //
        //   canExecute:
        //     The execution status logic. IMPORTANT: If the func causes a closure, you must
        //     set keepTargetAlive to true to avoid side effects.
        //
        //   keepTargetAlive:
        //     If true, the target of the Action will be kept as a hard reference, which might
        //     cause a memory leak. You should only set this parameter to true if the action
        //     is causing a closure. See http://galasoft.ch/s/mvvmweakaction.
        //
        // 异常:
        //   T:System.ArgumentNullException:
        //     If the execute argument is null.
        public RelayCommand(Action<T> execute, Func<T, bool> canExecute, bool keepTargetAlive = false)
        {
            if (execute == null)
            {
                throw new ArgumentNullException("execute");
            }

            _execute = new WeakAction<T>(execute, keepTargetAlive);
            if (canExecute != null)
            {
                _canExecute = new WeakFunc<T, bool>(canExecute, keepTargetAlive);
            }
        }

        //
        // 摘要:
        //     Raises the GalaSoft.MvvmLight.Command.RelayCommand`1.CanExecuteChanged event.
        public void RaiseCanExecuteChanged()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        //
        // 摘要:
        //     Defines the method that determines whether the command can execute in its current
        //     state.
        //
        // 参数:
        //   parameter:
        //     Data used by the command. If the command does not require data to be passed,
        //     this object can be set to a null reference
        //
        // 返回结果:
        //     true if this command can be executed; otherwise, false.
        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
            {
                return true;
            }

            if (_canExecute.IsStatic || _canExecute.IsAlive)
            {
                if (parameter == null && typeof(T).IsValueType)
                {
                    return _canExecute.Execute(default(T));
                }

                if (parameter == null || parameter is T)
                {
                    return _canExecute.Execute((T)parameter);
                }
            }

            return false;
        }

        //
        // 摘要:
        //     Defines the method to be called when the command is invoked.
        //
        // 参数:
        //   parameter:
        //     Data used by the command. If the command does not require data to be passed,
        //     this object can be set to a null reference
        public virtual void Execute(object parameter)
        {
            object obj = parameter;
            if (parameter != null && parameter.GetType() != typeof(T) && parameter is IConvertible)
            {
                obj = Convert.ChangeType(parameter, typeof(T), null);
            }

            if (!CanExecute(obj) || _execute == null || (!_execute.IsStatic && !_execute.IsAlive))
            {
                return;
            }

            if (obj == null)
            {
                if (typeof(T).IsValueType)
                {
                    _execute.Execute(default(T));
                }
                else
                {
                    _execute.Execute((T)obj);
                }
            }
            else
            {
                _execute.Execute((T)obj);
            }
        }
    }


}
