﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace datagrid.MVVM
{
    public interface IExecuteWithObjectAndResult
    {
        //
        // 摘要:
        //     Executes a Func and returns the result.
        //
        // 参数:
        //   parameter:
        //     A parameter passed as an object, to be casted to the appropriate type.
        //
        // 返回结果:
        //     The result of the operation.
        object ExecuteWithObject(object parameter);
    }
}
