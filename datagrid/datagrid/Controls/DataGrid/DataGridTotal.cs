﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace datagrid.Controls
{
    public class DataGridTotal
    {
        /// <summary>
        /// 内容
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 宽度
        /// </summary>
        public double Width { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// 标题头
        /// </summary>
        public string Header { get; set; }
    }
}
