﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;
using System.Windows.Shapes;

namespace datagrid.Events
{
    partial class DataGridEvents : ResourceDictionary
    {
        private void DataGridCell_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyboardDevice.IsKeyDown(Key.LeftCtrl) || e.KeyboardDevice.IsKeyDown(Key.RightCtrl)) && e.KeyboardDevice.IsKeyDown(Key.C))
            {

            }
        }
        private void DataGridCell_Click(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Button btn)
            {
                if (btn.Parent is System.Windows.Controls.Grid grid)
                {
                    if (grid.Children[0] is System.Windows.Controls.ContentPresenter pressenter)
                    {
                        if (pressenter.Content != null && pressenter.Content is System.Windows.Controls.TextBlock textblock)
                        {
                            if (textblock.Text != null)
                            {
                                try
                                {
                                    Clipboard.SetText(textblock.Text);
                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }
        }
        private void MenuItem_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is StackPanel wrap)
            {
                if (wrap.DataContext is Grid grid1)
                {

                    Controls.DataGrid dataGrid = grid1.Tag as Controls.DataGrid;
                    if (dataGrid != null)
                    {
                        //数据处理
                        if (grid1.TemplatedParent != null && grid1.TemplatedParent is DataGridColumnHeader header2)
                        {
                            if (header2 != null && header2.Column != null && header2.Column.SortMemberPath != null)
                            {
                                var columnStr = header2.Column.SortMemberPath;
                                var items = dataGrid.OriginItemsSource as IEnumerable<object>;
                                List<string> list = new List<string>();
                                foreach (var itemm in items)
                                {
                                    var getType = itemm.GetType().GetProperty(columnStr);
                                    if (getType != null)
                                    {
                                        var cont = getType.GetValue(itemm, null)?.ToString();
                                        if (!list.Contains(cont))
                                        {
                                            list.Add(cont);
                                        }
                                    }
                                }
                                list = list.Take(30).ToList();
                                wrap.Children.Clear();
                                CheckBox checkBoxall = new CheckBox();
                                checkBoxall.Content = "全部";
                                checkBoxall.FontSize = 14;
                                checkBoxall.Margin = new Thickness(5, 3, 5, 3);
                                wrap.Children.Add(checkBoxall);
                                foreach (var item in list)
                                {
                                    CheckBox checkBox = new CheckBox();
                                    checkBox.Content = item;
                                    checkBox.FontSize = 14;
                                    checkBox.Margin = new Thickness(5, 3, 5, 3);
                                    wrap.Children.Add(checkBox);
                                }
                            }
                        }

                        //显示过滤器图标
                        foreach (var item2 in dataGrid.Columns)
                        {
                            if (item2.Header is string header)
                            {

                                if (grid1.DataContext != null && grid1.DataContext is string str)
                                {
                                    if (str == header)
                                    {
                                        StackPanel stack = new StackPanel();
                                        TextBlock text = new TextBlock();
                                        text.FontSize = dataGrid.FontSize;
                                        text.VerticalAlignment = VerticalAlignment.Center;
                                        text.Text = header;
                                        text.Margin = new Thickness(0, 0, 4, 0);
                                        stack.Children.Add(text);
                                        Path path1 = new Path();
                                        path1.Tag = "filter";
                                        path1.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                        path1.Fill = System.Windows.Media.Brushes.Gray;
                                        path1.Stretch = System.Windows.Media.Stretch.Uniform;
                                        path1.Width = 12;
                                        path1.Visibility = Visibility.Visible;
                                        stack.Children.Add(path1);
                                        stack.HorizontalAlignment = HorizontalAlignment.Center;
                                        stack.Orientation = Orientation.Horizontal;
                                        item2.Header = stack;
                                    }
                                }

                            }
                            else if (item2.Header is StackPanel panel)
                            {

                                if (grid1.DataContext != null && grid1.DataContext is StackPanel sp)
                                {
                                    if (sp == panel)
                                    {
                                        bool hasfilter = false;
                                        for (int i = 0; i < panel.Children.Count; i++)
                                        {
                                            if (panel.Children[i] is Path path1)
                                            {
                                                if (path1.Tag != null && path1.Tag is string type)
                                                {
                                                    if (type == "filter")
                                                    {
                                                        hasfilter = true;
                                                        path1.Visibility = Visibility.Visible;
                                                    }
                                                }
                                            }
                                        }
                                        if (!hasfilter)
                                        {
                                            Path path2 = new Path();
                                            path2.Tag = "filter";
                                            path2.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                            path2.Fill = System.Windows.Media.Brushes.Gray;
                                            path2.Stretch = System.Windows.Media.Stretch.Uniform;
                                            path2.Width = 12;
                                            path2.Visibility = Visibility.Visible;
                                            panel.Children.Add(path2);
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 0; i < panel.Children.Count; i++)
                                        {
                                            if (panel.Children[i] is Path path1)
                                            {
                                                if (path1.Tag != null && path1.Tag is string type)
                                                {
                                                    if (type == "filter")
                                                    {
                                                        path1.Visibility = Visibility.Collapsed;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }


        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (sender is TextBox box)
                {
                    string content = box.Text;
                    if (box.Parent is Grid grid)
                    {
                        if (grid.TemplatedParent is MenuItem item)
                        {
                            if (item.Parent is MenuItem item1)
                            {
                                if (item1.Parent is ContextMenu menu)
                                {
                                    if (menu.DataContext is Grid grid1)
                                    {

                                        Controls.DataGrid dataGrid = grid1.Tag as Controls.DataGrid;
                                        if (dataGrid != null)
                                        {
                                            //数据处理
                                            if (grid1.TemplatedParent != null && grid1.TemplatedParent is DataGridColumnHeader header2)
                                            {
                                                if (header2 != null && header2.Column != null && header2.Column.SortMemberPath != null)
                                                {
                                                    var columnStr = header2.Column.SortMemberPath;
                                                    var items = dataGrid.OriginItemsSource as IEnumerable<object>;
                                                    List<object> newList = new List<object>();
                                                    foreach (var itemm in items)
                                                    {

                                                        var getType = itemm.GetType().GetProperty(columnStr);
                                                        if (getType != null)
                                                        {
                                                            var cont = getType.GetValue(itemm, null)?.ToString();
                                                            switch (box.Name)
                                                            {
                                                                case "equal"://等于
                                                                    if (cont == content)
                                                                        newList.Add(itemm);
                                                                    break;
                                                                case "noEqual"://不等于
                                                                    if (cont != content)
                                                                        newList.Add(itemm);
                                                                    break;
                                                                case "include"://包含
                                                                    if (cont.Contains(content))
                                                                        newList.Add(itemm);
                                                                    break;
                                                                case "greater"://大于等于
                                                                    if (cont != null)
                                                                    {

                                                                        if (DateTime.TryParse(cont, out DateTime date))
                                                                        {
                                                                            cont = date.ToString("yyyy-MM-dd HH:mm:ss");
                                                                        }

                                                                        string newCont = "";
                                                                        foreach (var item3 in cont)//数据原
                                                                        {
                                                                            if (int.TryParse(item3.ToString(), out int a))
                                                                            {
                                                                                newCont += a;
                                                                            }
                                                                        }
                                                                        string newContent = "";
                                                                        foreach (var item3 in content)//数据原
                                                                        {
                                                                            if (int.TryParse(item3.ToString(), out int a))
                                                                            {
                                                                                newContent += a;
                                                                            }
                                                                        }
                                                                        if (!string.IsNullOrEmpty(newCont) && !string.IsNullOrEmpty(newContent))
                                                                        {
                                                                            if (long.Parse(newCont) >= long.Parse(newContent))
                                                                            {
                                                                                newList.Add(itemm);
                                                                            }
                                                                        }
                                                                    }
                                                                    break;
                                                                case "less"://小于等于
                                                                    if (cont != null)
                                                                    {
                                                                        string newCont = "";
                                                                        foreach (var item3 in cont)//数据原
                                                                        {
                                                                            if (int.TryParse(item3.ToString(), out int a))
                                                                            {
                                                                                newCont += a;
                                                                            }
                                                                        }
                                                                        string newContent = "";
                                                                        foreach (var item3 in content)//数据原
                                                                        {
                                                                            if (int.TryParse(item3.ToString(), out int a))
                                                                            {
                                                                                newContent += a;
                                                                            }
                                                                        }
                                                                        if (!string.IsNullOrEmpty(newCont) && !string.IsNullOrEmpty(newContent))
                                                                        {
                                                                            if (long.Parse(newCont) <= long.Parse(newContent))
                                                                            {
                                                                                newList.Add(itemm);
                                                                            }
                                                                        }
                                                                    }
                                                                    break;
                                                                default:
                                                                    break;
                                                            }


                                                        }
                                                    }
                                                    dataGrid.ItemsSource = newList;

                                                }
                                            }

                                            //显示过滤器图标
                                            foreach (var item2 in dataGrid.Columns)
                                            {
                                                if (item2.Header is string header)
                                                {

                                                    if (grid1.DataContext != null && grid1.DataContext is string str)
                                                    {
                                                        if (str == header)
                                                        {
                                                            StackPanel stack = new StackPanel();
                                                            TextBlock text = new TextBlock();
                                                            text.FontSize = dataGrid.FontSize;
                                                            text.VerticalAlignment = VerticalAlignment.Center;
                                                            text.Text = header;
                                                            text.Margin = new Thickness(0, 0, 4, 0);
                                                            stack.Children.Add(text);
                                                            Path path1 = new Path();
                                                            path1.Tag = "filter";
                                                            path1.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                                            path1.Fill = System.Windows.Media.Brushes.Gray;
                                                            path1.Stretch = System.Windows.Media.Stretch.Uniform;
                                                            path1.Width = 12;
                                                            path1.Visibility = Visibility.Visible;
                                                            stack.Children.Add(path1);
                                                            stack.HorizontalAlignment = HorizontalAlignment.Center;
                                                            stack.Orientation = Orientation.Horizontal;
                                                            item2.Header = stack;
                                                        }
                                                    }

                                                }
                                                else if (item2.Header is StackPanel panel)
                                                {

                                                    if (grid1.DataContext != null && grid1.DataContext is StackPanel sp)
                                                    {
                                                        if (sp == panel)
                                                        {
                                                            bool hasfilter = false;
                                                            for (int i = 0; i < panel.Children.Count; i++)
                                                            {
                                                                if (panel.Children[i] is Path path1)
                                                                {
                                                                    if (path1.Tag != null && path1.Tag is string type)
                                                                    {
                                                                        if (type == "filter")
                                                                        {
                                                                            hasfilter = true;
                                                                            path1.Visibility = Visibility.Visible;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if (!hasfilter)
                                                            {
                                                                Path path2 = new Path();
                                                                path2.Tag = "filter";
                                                                path2.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                                                path2.Fill = System.Windows.Media.Brushes.Gray;
                                                                path2.Stretch = System.Windows.Media.Stretch.Uniform;
                                                                path2.Width = 12;
                                                                path2.Visibility = Visibility.Visible;
                                                                panel.Children.Add(path2);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            for (int i = 0; i < panel.Children.Count; i++)
                                                            {
                                                                if (panel.Children[i] is Path path1)
                                                                {
                                                                    if (path1.Tag != null && path1.Tag is string type)
                                                                    {
                                                                        if (type == "filter")
                                                                        {
                                                                            path1.Visibility = Visibility.Collapsed;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DataGridHeader_Click(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Button btn)
            {
                Controls.DataGrid dataGrid = btn.Tag as Controls.DataGrid;
                if (dataGrid != null && btn.Parent is System.Windows.Controls.Grid grid)
                {
                    Popup popup = grid.FindName("popup") as Popup;
                    if (popup != null && popup.IsOpen == false)
                    {

                        if (btn.TemplatedParent != null && btn.TemplatedParent is DataGridColumnHeader header)
                        {
                            if (header != null && header.Column != null && header.Column.SortMemberPath != null)
                            {
                                var columnStr = header.Column.SortMemberPath;
                                var items = dataGrid.OriginItemsSource as IEnumerable<object>;
                                List<string> list = new List<string>();
                                foreach (var item in items)
                                {
                                    var getType = item.GetType().GetProperty(columnStr);
                                    if (getType != null)
                                    {
                                        var cont = getType.GetValue(item, null).ToString();
                                        if (!list.Contains(cont))
                                            list.Add(cont);
                                    }
                                }
                                if (list.Count > 0)
                                {
                                    StackPanel stack = popup.FindName("panel") as StackPanel;
                                    stack.Children.Clear();
                                    CheckBox checkBox = new CheckBox();
                                    checkBox.Content = "全部";
                                    checkBox.FontSize = 14;
                                    stack.Children.Add(checkBox);
                                    list.ForEach(x => {
                                        CheckBox checkBox2 = new CheckBox();
                                        checkBox2.Margin = new Thickness(0, 3, 0, 3);
                                        checkBox2.Content = x;
                                        checkBox2.FontSize = 14;

                                        stack.Children.Add(checkBox2);
                                    });
                                    popup.IsOpen = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Exec_Click(object sender, RoutedEventArgs e)
        {
            if (sender is System.Windows.Controls.Button btn)
            {
                List<string> select = new List<string>();
                StackPanel stacks = btn.FindName("panel") as StackPanel;
                if (stacks != null && stacks.Children.Count > 0)
                {
                    foreach (var item in stacks.Children)
                    {
                        if (item is CheckBox check)
                        {
                            if (check.IsChecked == true)
                            {
                                select.Add(check.Content.ToString());
                            }
                        }
                    }
                }
                if (select.Count > 0)
                {
                    if (select.Contains("全部"))
                    {
                        if (stacks.DataContext != null && stacks.DataContext is Grid grid)
                        {
                            if (grid.Tag is Controls.DataGrid dataGrid)
                            {
                                var items = dataGrid.OriginItemsSource as IEnumerable<object>;
                                dataGrid.ItemsSource = items;
                                if (stacks.TemplatedParent is MenuItem itemm)
                                {
                                    if (itemm.Parent is MenuItem item1)
                                    {
                                        if (item1.Parent is ContextMenu menu)
                                        {
                                            menu.IsOpen = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (stacks.DataContext != null && stacks.DataContext is Grid grid)
                        {
                            if (grid.Tag is Controls.DataGrid dataGrid)
                            {
                                var items = dataGrid.OriginItemsSource as IEnumerable<object>;
                                if (items != null && items.Count() > 0)
                                {
                                    List<object> newlist = new List<object>();
                                    if (stacks.TemplatedParent is MenuItem itemm)
                                    {
                                        if (itemm.Parent is MenuItem item1)
                                        {
                                            if (item1.Parent is ContextMenu menu)
                                            {
                                                foreach (object item in items)
                                                {
                                                    foreach (var item2 in select)
                                                    {

                                                        if (grid.TemplatedParent != null && grid.TemplatedParent is DataGridColumnHeader header)
                                                        {
                                                            if (header != null && header.Column != null && header.Column.SortMemberPath != null)
                                                            {
                                                                var getType = item.GetType().GetProperty(header.Column.SortMemberPath);
                                                                if (getType != null)
                                                                {
                                                                    var cont = getType.GetValue(item, null).ToString();
                                                                    if (cont == item2)
                                                                    {
                                                                        //找到了
                                                                        newlist.Add(item);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                }

                                                dataGrid.ItemsSource = newlist;

                                                menu.IsOpen = false;
                                                if (menu.DataContext is Grid grid1)
                                                {
                                                    //显示过滤器图标
                                                    foreach (var item2 in dataGrid.Columns)
                                                    {
                                                        if (item2.Header is string header)
                                                        {

                                                            if (grid1.DataContext != null && grid1.DataContext is string str)
                                                            {
                                                                if (str == header)
                                                                {
                                                                    StackPanel stack = new StackPanel();
                                                                    TextBlock text = new TextBlock();
                                                                    text.FontSize = dataGrid.FontSize;
                                                                    text.VerticalAlignment = VerticalAlignment.Center;
                                                                    text.Text = header;
                                                                    text.Margin = new Thickness(0, 0, 4, 0);
                                                                    stack.Children.Add(text);
                                                                    Path path1 = new Path();
                                                                    path1.Tag = "filter";
                                                                    path1.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                                                    path1.Fill = System.Windows.Media.Brushes.Gray;
                                                                    path1.Stretch = System.Windows.Media.Stretch.Uniform;
                                                                    path1.Width = 12;
                                                                    path1.Visibility = Visibility.Visible;
                                                                    stack.Children.Add(path1);
                                                                    stack.HorizontalAlignment = HorizontalAlignment.Center;
                                                                    stack.Orientation = Orientation.Horizontal;
                                                                    item2.Header = stack;
                                                                }
                                                            }

                                                        }
                                                        else if (item2.Header is StackPanel panel)
                                                        {

                                                            if (grid1.DataContext != null && grid1.DataContext is StackPanel sp)
                                                            {
                                                                if (sp == panel)
                                                                {
                                                                    bool hasfilter = false;
                                                                    for (int i = 0; i < panel.Children.Count; i++)
                                                                    {
                                                                        if (panel.Children[i] is Path path1)
                                                                        {
                                                                            if (path1.Tag != null && path1.Tag is string type)
                                                                            {
                                                                                if (type == "filter")
                                                                                {
                                                                                    hasfilter = true;
                                                                                    path1.Visibility = Visibility.Visible;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (!hasfilter)
                                                                    {
                                                                        Path path2 = new Path();
                                                                        path2.Tag = "filter";
                                                                        path2.Data = System.Windows.Media.Geometry.Parse("M811.525 99H206.472C174.732 99 149 124.729 149 156.467l0.012 1.151a57.463 57.463 0 0 0 11.911 33.893L403.1 506.216l0.001 276.645a80.452 80.452 0 0 0 42.561 70.969l106.78 57.013c21.28 11.362 47.742 3.323 59.105-17.954l0.52-1.006a43.671 43.671 0 0 0 4.629-19.566V504.993l240.438-313.56c19.313-25.187 14.55-61.26-10.64-80.571A57.476 57.476 0 0 0 811.524 99z");
                                                                        path2.Fill = System.Windows.Media.Brushes.Gray;
                                                                        path2.Stretch = System.Windows.Media.Stretch.Uniform;
                                                                        path2.Width = 12;
                                                                        path2.Visibility = Visibility.Visible;
                                                                        panel.Children.Add(path2);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    for (int i = 0; i < panel.Children.Count; i++)
                                                                    {
                                                                        if (panel.Children[i] is Path path1)
                                                                        {
                                                                            if (path1.Tag != null && path1.Tag is string type)
                                                                            {
                                                                                if (type == "filter")
                                                                                {
                                                                                    path1.Visibility = Visibility.Collapsed;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

            }
        }

    }

}
