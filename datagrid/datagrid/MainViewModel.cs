﻿using datagrid.Model;
using datagrid.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace datagrid
{
    public class MainViewModel:ViewModelBase
    {
        #region 界面属性
        private TicketSalesQuery totalItems;

        public TicketSalesQuery TotalItems
        {
            get { return totalItems; }
            set { totalItems = value; RaisePropertyChanged("TotalItems"); }
        }
        private List<TicketSalesQuery> ticketSalesQueryResource;
        /// <summary>
        /// 车票查询列表
        /// </summary>
        public List<TicketSalesQuery> TicketSalesQuerySource
        {
            get { return ticketSalesQueryResource; }
            set { ticketSalesQueryResource = value; RaisePropertyChanged("TicketSalesQuerySource"); }
        }
        #endregion


        public MainViewModel() { 
        var ticketList = new List<TicketSalesQuery>();
            for (int i = 0; i < 10000; i++)
            {
                ticketList.Add(new TicketSalesQuery
                {
                    ArriveStation = "北京" + i,
                    CardNo = new Random().Next(10000).ToString(),
                    CheckState = "未检",
                    ClassCode = new Random().Next(100).ToString(),
                    FactStartTime = "19:30",
                    FreeTicketNum = 1,
                    GetOnStation = "成都",
                    LeaveDate = "2023-01-01",
                    Name = "张三" + i,
                    SeatNo = i.ToString(),
                    LineName = "成都-北京",
                    Phone = new Random().Next(10000000).ToString(),
                }) ;
                TicketSalesQuerySource = ticketList;
                TotalItems = ticketList.First();
            }
        }
    }
}
