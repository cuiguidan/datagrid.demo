﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;

namespace datagrid.Assist
{
    public static class DataGridAssist
    {
        private static readonly Thickness DefaultCellPaddingThickness = new Thickness(3, 0, 3, 0);
        private static readonly Thickness DefaultColumnHeaderPadding = new Thickness(16, 0, 16, 0);
        private static readonly CornerRadius DefaultCornerRadius = new CornerRadius(4);
        /// <summary>
        /// 设置行高默认30
        /// </summary>
        private static readonly double DefaultCellHeight = 27;
        /// <summary>
        /// 标题栏高
        /// </summary>
        private static readonly double DefaultHeaderHeight = 30;

        private static readonly Thickness DefaultCellBorderThickness = new Thickness(1, 0, 0, 0);
        private static readonly Thickness DefaultHeaderBorderThickness = new Thickness(1, 0, 0, 1);
        private static readonly Brush DefaultHeaderBackground = Brushes.Transparent;
        #region AttachedProperty : CellPaddingProperty
        public static readonly DependencyProperty CellPaddingProperty
            = DependencyProperty.RegisterAttached("CellPadding", typeof(Thickness), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultCellPaddingThickness, FrameworkPropertyMetadataOptions.Inherits));

        public static Thickness GetCellPadding(DataGrid element)
            => (Thickness)element.GetValue(CellPaddingProperty);
        public static void SetCellPadding(DataGrid element, Thickness value)
            => element.SetValue(CellPaddingProperty, value);
        #endregion
        #region AttachedProperty : 行内框框展示
        public static readonly DependencyProperty CellBorderThicknessProperty
            = DependencyProperty.RegisterAttached("CellBorderThickness", typeof(Thickness), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultCellBorderThickness, FrameworkPropertyMetadataOptions.Inherits));

        public static Thickness GetCellBorderThickness(DataGrid element)
            => (Thickness)element.GetValue(CellBorderThicknessProperty);
        public static void SetCellBorderThickness(DataGrid element, Thickness value)
            => element.SetValue(CellBorderThicknessProperty, value);
        #endregion
        #region AttachedProperty : 标题栏背景颜色
        public static readonly DependencyProperty HeaderBackgroundProperty
            = DependencyProperty.RegisterAttached("HeaderBackground", typeof(Brush), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultHeaderBackground, FrameworkPropertyMetadataOptions.Inherits));

        public static Brush GetHeaderBackground(DataGrid element)
            => (Brush)element.GetValue(HeaderBackgroundProperty);
        public static void SetHeaderBackground(DataGrid element, Brush value)
            => element.SetValue(HeaderBackgroundProperty, value);
        #endregion
        #region AttachedProperty : 标题栏框框展示
        public static readonly DependencyProperty HeaderBorderThicknessProperty
            = DependencyProperty.RegisterAttached("HeaderBorderThickness", typeof(Thickness), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultHeaderBorderThickness, FrameworkPropertyMetadataOptions.Inherits));

        public static Thickness GetHeaderBorderThickness(DataGrid element)
            => (Thickness)element.GetValue(HeaderBorderThicknessProperty);
        public static void SetHeaderBorderThickness(DataGrid element, Thickness value)
            => element.SetValue(HeaderBorderThicknessProperty, value);
        #endregion
        #region DataGrid行高
        public static readonly DependencyProperty CellHeightProperty
            = DependencyProperty.RegisterAttached("CellHeight", typeof(double), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultCellHeight, FrameworkPropertyMetadataOptions.Inherits));

        public static double GetCellHeight(DataGrid element)
            => (double)element.GetValue(CellHeightProperty);
        public static void SetCellHeight(DataGrid element, double value)
            => element.SetValue(CellHeightProperty, value);
        #endregion
        #region DataGrid标题高度
        public static readonly DependencyProperty HeaderHeightProperty
            = DependencyProperty.RegisterAttached("HeaderHeight", typeof(double), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultHeaderHeight, FrameworkPropertyMetadataOptions.Inherits));

        public static double GetHeaderHeight(DataGrid element)
            => (double)element.GetValue(HeaderHeightProperty);
        public static void SetHeaderHeight(DataGrid element, double value)
            => element.SetValue(HeaderHeightProperty, value);
        #endregion

        #region AttachedProperty : ColumnHeaderPaddingProperty
        public static readonly DependencyProperty ColumnHeaderPaddingProperty
            = DependencyProperty.RegisterAttached("ColumnHeaderPadding", typeof(Thickness), typeof(DataGridAssist),
                new FrameworkPropertyMetadata(DefaultColumnHeaderPadding, FrameworkPropertyMetadataOptions.Inherits));

        public static Thickness GetColumnHeaderPadding(DataGrid element)
            => (Thickness)element.GetValue(ColumnHeaderPaddingProperty);
        public static void SetColumnHeaderPadding(DependencyObject element, Thickness value)
            => element.SetValue(ColumnHeaderPaddingProperty, value);
        #endregion


        #region AttachedProperty : AutoGeneratedCheckboxStyleProperty(弃用中)
        /*
        public static readonly DependencyProperty AutoGeneratedCheckBoxStyleProperty
            = DependencyProperty.RegisterAttached("AutoGeneratedCheckBoxStyle", typeof(Style), typeof(DataGridAssist),
                new PropertyMetadata(default(Style), AutoGeneratedCheckBoxStylePropertyChangedCallback));

        public static Style GetAutoGeneratedCheckBoxStyle(DataGrid element)
            => (Style)element.GetValue(AutoGeneratedCheckBoxStyleProperty);
        public static void SetAutoGeneratedCheckBoxStyle(DataGrid element, Style value)
            => element.SetValue(AutoGeneratedCheckBoxStyleProperty, value);

        private static void AutoGeneratedCheckBoxStylePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (DataGrid)d;

            dataGrid.AutoGeneratingColumn -= SetGeneratedCheckboxColumnStyle;
            dataGrid.AutoGeneratingColumn += SetGeneratedCheckboxColumnStyle;
        }

        private static void SetGeneratedCheckboxColumnStyle(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column is DataGridCheckBoxColumn column &&
                sender is DataGrid dataGrid)
            {
                column.ElementStyle = GetAutoGeneratedCheckBoxStyle(dataGrid);
            }
        }
        */
        #endregion

        #region AttachedProperty : AutoGeneratedEditingCheckBoxStyleProperty(弃用中)
        /*
        public static readonly DependencyProperty AutoGeneratedEditingCheckBoxStyleProperty
            = DependencyProperty.RegisterAttached("AutoGeneratedEditingCheckBoxStyle", typeof(Style), typeof(DataGridAssist),
                new PropertyMetadata(default(Style), AutoGeneratedEditingCheckBoxStylePropertyChangedCallback));

        public static Style GetAutoGeneratedEditingCheckBoxStyle(DataGrid element)
            => (Style)element.GetValue(AutoGeneratedEditingCheckBoxStyleProperty);
        public static void SetAutoGeneratedEditingCheckBoxStyle(DataGrid element, Style value)
            => element.SetValue(AutoGeneratedEditingCheckBoxStyleProperty, value);

        private static void AutoGeneratedEditingCheckBoxStylePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (DataGrid)d;

            dataGrid.AutoGeneratingColumn -= SetGeneratedCheckBoxEditingStyle;
            dataGrid.AutoGeneratingColumn += SetGeneratedCheckBoxEditingStyle;
        }

        private static void SetGeneratedCheckBoxEditingStyle(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column is DataGridCheckBoxColumn column &&
                sender is DataGrid dataGrid)
            {
                column.EditingElementStyle = GetAutoGeneratedEditingCheckBoxStyle(dataGrid);
            }
        }
        */
        #endregion

        #region AttachedProperty : AutoGeneratedTextStyleProperty(弃用中)
        /*
        public static readonly DependencyProperty AutoGeneratedTextStyleProperty
            = DependencyProperty.RegisterAttached("AutoGeneratedTextStyle", typeof(Style), typeof(DataGridAssist),
                new PropertyMetadata(default(Style), AutoGeneratedTextStylePropertyChangedCallback));

        public static Style GetAutoGeneratedTextStyle(DataGrid element)
            => (Style)element.GetValue(AutoGeneratedTextStyleProperty);
        public static void SetAutoGeneratedTextStyle(DataGrid element, Style value)
            => element.SetValue(AutoGeneratedTextStyleProperty, value);

        private static void AutoGeneratedTextStylePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (DataGrid)d;

            dataGrid.AutoGeneratingColumn -= SetGeneratedTextColumnStyle;
            dataGrid.AutoGeneratingColumn += SetGeneratedTextColumnStyle;
        }

        private static void SetGeneratedTextColumnStyle(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column is System.Windows.Controls.DataGridTextColumn column &&
                sender is DataGrid dataGrid)
            {
                column.ElementStyle = GetAutoGeneratedTextStyle(dataGrid);
            }
        }
        */
        #endregion

        #region AttachedProperty : AutoGeneratedEditingTextStyleProperty(弃用中)
        /*
        public static readonly DependencyProperty AutoGeneratedEditingTextStyleProperty
            = DependencyProperty.RegisterAttached("AutoGeneratedEditingTextStyle", typeof(Style), typeof(DataGridAssist),
                new PropertyMetadata(default(Style), AutoGeneratedEditingTextStylePropertyChangedCallback));

        public static Style GetAutoGeneratedEditingTextStyle(DataGrid element)
            => (Style)element.GetValue(AutoGeneratedEditingTextStyleProperty);
        public static void SetAutoGeneratedEditingTextStyle(DataGrid element, Style value)
            => element.SetValue(AutoGeneratedEditingTextStyleProperty, value);

        private static void AutoGeneratedEditingTextStylePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (DataGrid)d;

            dataGrid.AutoGeneratingColumn -= SetGeneratedTextColumnEditingStyle;
            dataGrid.AutoGeneratingColumn += SetGeneratedTextColumnEditingStyle;
        }

        private static void SetGeneratedTextColumnEditingStyle(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.Column is System.Windows.Controls.DataGridTextColumn column &&
                sender is DataGrid dataGrid)
            {
                column.EditingElementStyle = GetAutoGeneratedEditingTextStyle(dataGrid);
            }
        }
        */
        #endregion

        #region AttachedProperty : EnableEditBoxAssistProperty(弃用中)
        /*
        public static readonly DependencyProperty EnableEditBoxAssistProperty
            = DependencyProperty.RegisterAttached("EnableEditBoxAssist", typeof(bool), typeof(DataGridAssist),
                new PropertyMetadata(default(bool), EnableEditBoxAssistPropertyChangedCallback));

        public static bool GetEnableEditBoxAssist(DataGrid element)
            => (bool)element.GetValue(EnableEditBoxAssistProperty);
        public static void SetEnableEditBoxAssist(DataGrid element, bool value)
            => element.SetValue(EnableEditBoxAssistProperty, value);

        private static void EnableEditBoxAssistPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = (DataGrid)d;
            var enableCheckBoxAssist = (bool)e.NewValue;

            if (enableCheckBoxAssist)
            {
                dataGrid.PreviewMouseLeftButtonDown += AllowDirectEditWithoutFocus;
                dataGrid.KeyDown += EditOnSpacebarPress;
            }
            else
            {
                dataGrid.PreviewMouseLeftButtonDown -= AllowDirectEditWithoutFocus;
                dataGrid.KeyDown -= EditOnSpacebarPress;
            }
        }
        */
        #endregion

        #region AttachedProperty : CornerRadiusProperty
        public static readonly DependencyProperty CornerRadiusProperty
            = DependencyProperty.RegisterAttached("CornerRadius", typeof(CornerRadius), typeof(DataGridAssist),
                new PropertyMetadata(DefaultCornerRadius));

        public static CornerRadius GetCornerRadius(DataGrid element)
            => (CornerRadius)element.GetValue(CornerRadiusProperty);
        public static void SetCornerRadius(DataGrid element, CornerRadius value)
            => element.SetValue(CornerRadiusProperty, value);
        #endregion
        /*
        private static void EditOnSpacebarPress(object sender, KeyEventArgs e)
        {
            var dataGrid = (DataGrid)sender;

            if (e.Key == Key.Space && e.OriginalSource is DataGridCell cell
                && !cell.IsReadOnly && cell.Column is DataGridComboBoxColumn)
            {
                dataGrid.BeginEdit();
            }
        }
        */
        /// <summary>
        /// Allows editing of components inside of a datagrid cell with a single left click.
        /// </summary>
        /*
        private static void AllowDirectEditWithoutFocus(object sender, MouseButtonEventArgs mouseArgs)
        {
            var originalSource = (DependencyObject)mouseArgs.OriginalSource;
            var dataGridCell = originalSource
                .GetVisualAncestry()
                .OfType<DataGridCell>()
                .FirstOrDefault();

            // Readonly has to be handled as the passthrough ignores the
            // cell and interacts directly with the content
            if (dataGridCell?.IsReadOnly ?? true)
            {
                return;
            }

            if (dataGridCell?.Content is UIElement element)
            {
                var dataGrid = (DataGrid)sender;

                // Check if the cursor actually hit the element and not just the cell
                var mousePosition = mouseArgs.GetPosition(element);
                var elementHitBox = new Rect(element.RenderSize);
                if (elementHitBox.Contains(mousePosition))
                {
                    // If it is a DataGridTemplateColumn we want the
                    // click to be handled naturally by the control
                    if (dataGridCell.Column.GetType() == typeof(DataGridTemplateColumn))
                    {
                        return;
                    }

                    dataGrid.CurrentCell = new DataGridCellInfo(dataGridCell);
                    dataGrid.BeginEdit();
                    //Begin edit likely changes the visual tree, trigger the mouse down event to cause the DataGrid to adjust selection
                    var mouseDownEvent = new MouseButtonEventArgs(mouseArgs.MouseDevice, mouseArgs.Timestamp, mouseArgs.ChangedButton)
                    {
                        RoutedEvent = Mouse.MouseDownEvent,
                        Source = mouseArgs.Source
                    };

                    dataGridCell.RaiseEvent(mouseDownEvent);

                    switch (dataGridCell?.Content)
                    {
                        // Send a 'left click' routed command to the toggleButton
                        case ToggleButton toggleButton:
                            {
                                var newMouseEvent = new MouseButtonEventArgs(mouseArgs.MouseDevice, 0, MouseButton.Left)
                                {
                                    RoutedEvent = Mouse.MouseDownEvent,
                                    Source = dataGrid
                                };

                                toggleButton.RaiseEvent(newMouseEvent);
                                break;
                            }

                        // Open the dropdown explicitly. Left clicking is not
                        // viable, as it would edit the text and not open the
                        // dropdown
                        case ComboBox comboBox:
                            {
                                comboBox.IsDropDownOpen = true;
                                mouseArgs.Handled = true;
                                break;
                            }

                        default:
                            {
                                break;
                            }
                    }
                }
            }
        }
        /// <summary>
        /// Returns full visual ancestry, starting at the leaf.
        /// <para>If element is not of <see cref="Visual"/> or <see cref="Visual3D"/> the
        /// logical ancestry is used.</para>
        /// </summary>
        /// <param name="leaf"></param>
        /// <returns></returns>
        public static IEnumerable<DependencyObject> GetVisualAncestry(this DependencyObject leaf)
        {
            while (leaf != null)
            {
                yield return leaf;
                leaf = leaf is Visual || leaf is Visual3D
                    ? VisualTreeHelper.GetParent(leaf)
                    : LogicalTreeHelper.GetParent(leaf);
            }
        }*/
    }

}
