﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace datagrid.UIConvert
{
    public class HorizontalThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Thickness thickness)
            {
                return new Thickness(thickness.Left, 0, thickness.Right, 0);
            }
            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class RemoveAlphaBrushConverter : IValueConverter, IMultiValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => value is SolidColorBrush brush
                ? new SolidColorBrush(RgbaToRgb(brush.Color, parameter))
                : Binding.DoNothing;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            => values[0] is SolidColorBrush brush
                ? new SolidColorBrush(RgbaToRgb(brush.Color, values[1]))
                : Binding.DoNothing;

        private static Color RgbaToRgb(Color rgba, object background)
        {

            var backgroundColor = Colors.White;

            var alpha = (double)rgba.A / byte.MaxValue;
            var alphaReverse = 1 - alpha;

            return Color.FromRgb(
                (byte)(alpha * rgba.R + alphaReverse * backgroundColor.R),
                (byte)(alpha * rgba.G + alphaReverse * backgroundColor.G),
                (byte)(alpha * rgba.B + alphaReverse * backgroundColor.B)
            );
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
    public class NotZeroToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double val;
            double.TryParse((value ?? "").ToString(), out val);

            return Math.Abs(val) > 0.0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
    public class TopThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => value is Thickness thickness
                ? new Thickness(0, thickness.Top, 0, 0)
                : Binding.DoNothing;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => throw new NotImplementedException();
    }

    public class GridLinesVisibilityBorderToThicknessConverter : IValueConverter
    {
        private const double GridLinesThickness = 1;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is DataGridGridLinesVisibility visibility))
                return Binding.DoNothing;

            var thickness = parameter as double? ?? GridLinesThickness;

            switch (visibility)
            {
                case DataGridGridLinesVisibility.All:
                    return new Thickness(0, 0, thickness, thickness);
                case DataGridGridLinesVisibility.Horizontal:
                    return new Thickness(0, 0, 0, thickness);
                case DataGridGridLinesVisibility.None:
                    return new Thickness(0, 0, thickness, 0);
                case DataGridGridLinesVisibility.Vertical:
                    return new Thickness(0);
                default:
                    throw new ArgumentOutOfRangeException();
            }


        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => throw new NotSupportedException();
    }

    public class TreeLeftMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double colunwidth = 16;
            double left = 0.0;

            UIElement element = value as TreeViewItem;
            while (element.GetType() != typeof(TreeView))
            {
                element = (UIElement)VisualTreeHelper.GetParent(element);
                if (element.GetType() == typeof(TreeViewItem))
                    left += colunwidth;
            }
            return new Thickness(left, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// 安检项配置根据0和1判断true或者false
    /// </summary>
    public class CheckboxLicnesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string)
            {
                if (!string.IsNullOrEmpty(value as string))
                {
                    if (value.ToString() == "0")
                    {
                        return "False";
                    }
                    else if (value.ToString() == "1")
                    {
                        return "True";

                    }
                    else if (value.ToString() == "False")
                    {
                        return "0";

                    }
                    else
                    {
                        return "1";
                    }
                }
                else
                {
                    return "False";
                }
            }
            else
            {
                return "False";
            }
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }


    }

    /// <summary>
    /// 文字颜色改变
    /// </summary>
    public class LastAjTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string)
            {
                DateTime date = new DateTime();

                if (DateTime.TryParse(value as string, out date))
                {

                    if (date > DateTime.Today)
                    {
                        return "Visible";
                    }
                }
                return "Collapsed";
            }
            else
            {
                return "Collapsed";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// 文字颜色改变
    /// </summary>
    public class CardTime5Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string)
            {
                if (!string.IsNullOrEmpty(value as string))
                {

                    if (value.ToString() == "不合格")
                    {

                        return "#FF5555";
                    }
                    else
                    {
                        return "#3BD072";
                    }
                }
                else
                {
                    return "#3BD072";
                }
            }
            else
            {
                return "#3BD072";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    //小图标显示
    public class CardTime3Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string)
            {
                if (!string.IsNullOrEmpty(value as string))
                {

                    if (value.ToString() == "合格")
                    {

                        return "pack://application:,,,/Cloud.UI;component/Res/Img/confirmIcon.png";
                    }
                    else
                    {
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/errorIcon.png";
                    }
                }
                else
                {
                    return "pack://application:,,,/Cloud.UI;component/Res/Img/confirmIcon.png";
                }
            }
            else
            {
                return "pack://application:,,,/Cloud.UI;component/Res/Img/confirmIcon.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// 证照检查中边框颜色改变
    /// </summary>
    public class BorderThick2Convert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value?.ToString() == "1") return "#f15a53";
            return "#E5E5E5";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    /// <summary>
    /// 证照检查 安检情况
    /// </summary>
    public class AjConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string str)
            {
                if (str == "合格" || str == "-")
                {
                    return "black";
                }
                else
                {
                    return "red";
                }
            }
            else
            {
                return "red";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// 证照检查 安检情况
    /// </summary>
    public class TicketState2BGBySeatConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string state)
            {
                switch (state)
                {
                    case "0":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-空.png";
                    case "1":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-售.png";
                    case "2":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-检.png";
                    case "3":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-调度锁.png";
                    case "4":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-售票锁.png";
                    case "6":
                        return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-虚拟.png";
                    default: return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-空.png";
                }
            }
            return "pack://application:,,,/Cloud.UI;component/Res/Img/座位状态-空.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// TreeView左间距
    /// </summary>
    public class IndentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double colunwidth = 16;
            double left = 0.0;

            UIElement element = value as TreeViewItem;
            while (element.GetType() != typeof(TreeView))
            {
                element = (UIElement)VisualTreeHelper.GetParent(element);
                if (element.GetType() == typeof(TreeViewItem))
                    left += colunwidth;
            }
            return new Thickness(left, 0, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// 返回checkbox状态
    /// </summary>
    public class AllCheckBoxWithBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is bool)
            {
                if ((bool)value)
                    return new BitmapImage(new Uri("pack://application:,,,/Cloud.UI;component/Res/Img/all_check.png"));
                else
                    return new BitmapImage(new Uri("pack://application:,,,/Cloud.UI;component/Res/Img/all_uncheck.png"));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }


    /// <summary>
    /// 票种转换 携童->全票 全票->携童
    /// </summary>
    public class ConvertingWithChildrenConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string str)
            {
                if (str == "全票")
                {
                    return "携童票";
                }
                else if (str.Contains("携童"))
                {
                    return "全票";
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class ConvertingWithChildrenBorderBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is string str)
            {
                if (str == "全票")
                {
                    return "#017EFA";
                }
                else if (str.Contains("携童"))
                {
                    return "#26BC59";
                }
                else
                {
                    return "gray";
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    public class BoolenReConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is bool boo)
            {
                return !boo;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}

