﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace datagrid.Model
{
    public class TicketSalesQuery
    {
        /// <summary>
        /// 票号
        /// </summary>
        public string TicketNo { get; set; }
        /// <summary>
        /// 电子客票号
        /// </summary>
        public string NetTicketId { get; set; }
        /// <summary>
        /// 车票状态
        /// </summary>
        public string TicketState { get; set; }
        /// <summary>
        /// 票价
        /// </summary>
        public decimal? TicketPrice { get; set; }
        /// <summary>
        /// 保险金额
        /// </summary>
        public decimal? InsuranceFee { get; set; }
        /// <summary>
        /// 发车日期
        /// </summary>
        public string LeaveDate { get; set; }
        /// <summary>
        /// 发车时间
        /// </summary>
        public string FactStartTime { get; set; }
        /// <summary>
        /// 线路编码+“空格占位符”+线路终点站；
        /// </summary>
        public string LineName { get; set; }
        /// <summary>
        /// 班次编码
        /// </summary>
        public string ClassCode { get; set; }
        /// <summary>
        /// 座位号
        /// </summary>
        public string SeatNo { get; set; }
        /// <summary>
        /// 售票人数
        /// </summary>
        public int? SellNum { get; set; }
        /// <summary>
        /// 免票人数
        /// </summary>
        public int? FreeTicketNum { get; set; }
        /// <summary>
        /// 保险分数
        /// </summary>
        public int? InsuranceNum { get; set; }
        /// <summary>
        /// 退票手续费
        /// </summary>
        public decimal? RefundFee { get; set; }
        /// <summary>
        /// 退票手续费率
        /// </summary>
        public string RefundRate { get; set; }
        /// <summary>
        /// 车票类型：全票、半票、免票
        /// </summary>
        public string TicketType { get; set; }
        /// <summary>
        /// 检票状态：未检、已检、混检
        /// </summary>
        public string CheckState { get; set; }
        /// <summary>
        /// 重打次数
        /// </summary>
        public int? ReprintNum { get; set; }
        /// <summary>
        /// 售票渠道名称
        /// </summary>
        public string SellChannelName { get; set; }
        /// <summary>
        /// 售票员名称
        /// </summary>
        public string TicketSellerName { get; set; }
        /// <summary>
        /// 售票时间 yyyy-MM-dd HH:mm;ss
        /// </summary>
        public string SellTime { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        public string Modificator { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public string MoDate { get; set; }
        /// <summary>
        /// 上车站点名称
        /// </summary>
        public string GetOnStation { get; set; }
        /// <summary>
        /// 到达站名称
        /// </summary>
        public string ArriveStation { get; set; }
        /// <summary>
        /// 支付方式：现金支付、移动支付
        /// </summary>
        public string PayType { get; set; }
        /// <summary>
        /// 票种类型：普通、军残、学生、人才
        /// </summary>
        public string PaperType { get; set; }
        /// <summary>
        /// 乘客姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 证件号码
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 订单号：只展示网售订单号
        /// </summary>
        public string WebOrderId { get; set; }
    }

}
